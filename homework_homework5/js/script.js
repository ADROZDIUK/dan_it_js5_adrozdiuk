$(document).ready(function(){
    $(`body`).append($('<table id="main-table"><tbody id="main-table__body"></tbody></table>'));
    for(let i = 0; i < 50; i++){
        $(`#main-table__body`).append($('<tr></tr>').addClass(`main-table__body_row${i+1}`));
        for(let j = 0; j < 50; j++) {
            $(`.main-table__body_row${i+1}`).append($('<td></td>').addClass(`body_row${i+1}_td${j+1}`));
        }
    }

    class Tank {
        constructor(color,name){
            try {
                this.color = color;
                this.name = name;
            } catch(e) {
                console.log(e);
            }
        }

        runTop() {
            if(
                (!$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).hasClass("tank")   || $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).hasClass(`${this.name}`))  && !$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum+1}`).hasClass("tank") || $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum+1}`).hasClass(`${this.name}`))&& !$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum+1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum-1}`).hasClass("tank") || $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum-1}`).hasClass(`${this.name}`))&& !$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum-1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum-2}_td${this.tankTdNum}`).hasClass("tank")   || $(`.body_row${this.tankRowNum-2}_td${this.tankTdNum}`).hasClass(`${this.name}`))  && !$(`.body_row${this.tankRowNum-2}_td${this.tankTdNum}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass("tank")   || $(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass(`${this.name}`))  && !$(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass("tank")   || $(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass(`${this.name}`))  && !$(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass("barrier") &&
                (this.tankRowNum >= 3)) {
                    $(`.${this.name}`).removeClass(`${this.name}`).removeClass(`tank`).css("background-color",`white`);
                    this.tankMark = $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`);
                    this.tankMark = $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum+1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum-1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum-2}_td${this.tankTdNum}`);
                    this.tankMark = $(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`);
                    this.tankRowNum--;
            }
        }
    
        runBottom() {
            if(
                (!$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`).hasClass("tank")   || $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`).hasClass(`${this.name}`))  && !$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`).hasClass("tank") || $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`).hasClass(`${this.name}`))&& !$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`).hasClass("tank") || $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`).hasClass(`${this.name}`))&& !$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum+2}_td${this.tankTdNum}`).hasClass("tank")   || $(`.body_row${this.tankRowNum+2}_td${this.tankTdNum}`).hasClass(`${this.name}`))  && !$(`.body_row${this.tankRowNum+2}_td${this.tankTdNum}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass("tank")   || $(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass(`${this.name}`))  && !$(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass("tank")   || $(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass(`${this.name}`))  && !$(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass("barrier") &&
                (this.tankRowNum <= 48)) {
                    $(`.${this.name}`).removeClass(`${this.name}`).removeClass(`tank`).css("background-color",`white`);
                    this.tankMark = $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`);
                    this.tankMark = $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum+2}_td${this.tankTdNum}`);
                    this.tankMark = $(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`);
                    this.tankRowNum++;
            }
        }
    
        runLeft() {
            if(
                ($(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass("tank")    || !$(`.body_row${this.tankRowNum}_td${this.tankTdNu-1}`).hasClass(`${this.name}`))   && !$(`.body_row${this.tankRowNum}_td${this.tankTdNu-1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`).hasClass("tank") || $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`).hasClass(`${this.name}`)) && !$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum-1}`).hasClass("tank") || $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum-1}`).hasClass(`${this.name}`)) && !$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum-1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum}_td${this.tankTdNum-2}`).hasClass("tank")   || $(`.body_row${this.tankRowNum}_td${this.tankTdNum-2}`).hasClass(`${this.name}`))   && !$(`.body_row${this.tankRowNum}_td${this.tankTdNum-2}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`).hasClass("tank")   || $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`).hasClass(`${this.name}`))   && !$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).hasClass("tank")   || $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).hasClass(`${this.name}`))   && !$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).hasClass("barrier") &&
                (this.tankTdNum >= 3)) {
                    $(`.${this.name}`).removeClass(`${this.name}`).removeClass(`tank`).css("background-color",`white`);
                    this.tankMark = $(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum-1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum}_td${this.tankTdNum-2}`);
                    this.tankMark = $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`);
                    this.tankMark = $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`);
                    this.tankTdNum--;
            } 
        }
    
        runRight() {
            if(
                ($(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass("tank")    || !$(`.body_row${this.tankRowNum}_td${this.tankTdNu+1}`).hasClass(`${this.name}`))   && !$(`.body_row${this.tankRowNum}_td${this.tankTdNu+1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`).hasClass("tank") || $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`).hasClass(`${this.name}`)) && !$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum+1}`).hasClass("tank") || $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum+1}`).hasClass(`${this.name}`)) && !$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum+1}`).hasClass("barrier") && 
                (!$(`.body_row${this.tankRowNum}_td${this.tankTdNum+2}`).hasClass("tank")   || $(`.body_row${this.tankRowNum}_td${this.tankTdNum+2}`).hasClass(`${this.name}`))   && !$(`.body_row${this.tankRowNum}_td${this.tankTdNum+2}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`).hasClass("tank")   || $(`.body_row${this.tankRowNum}_td${this.tankTdNum}`).hasClass(`${this.name}`))     && !$(`.body_row${this.tankRowNum}_td${this.tankTdNum}`).hasClass("barrier") &&
                (!$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).hasClass("tank")   || $(`.body_row${this.tankRowNum}_td${this.tankTdNum}`).hasClass(`${this.name}`))     && !$(`.body_row${this.tankRowNum}_td${this.tankTdNum}`).hasClass("barrier") &&
                (this.tankTdNum <= 48)) {
                    $(`.${this.name}`).removeClass(`${this.name}`).removeClass(`tank`).css("background-color",`white`);
                    this.tankMark = $(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum+1}`);
                    this.tankMark = $(`.body_row${this.tankRowNum}_td${this.tankTdNum+2}`);
                    this.tankMark = $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`);
                    this.tankMark = $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`);
                    this.tankTdNum++;
            } 
        }

        set tankShell(direction) {
            let name = this.name;
            let shellRowNum = this.tankRowNum;
            let shellTdNum = this.tankTdNum;
            let color = this.color;

            if(direction === "top"){
                shellRowNum--;
                let topTimer = setInterval(function(){
                    shellRowNum--;
                    if($(`.body_row${shellRowNum}_td${shellTdNum}`).hasClass("tank") || shellRowNum === 1 || $(`.body_row${shellRowNum}_td${shellTdNum}`).hasClass("barrier")){
                        clearInterval(topTimer);
                        checkTank();
                        checkBarrier(); 
                    }
                    runShell();
                }, 45);

            } else if(direction === "bottom"){
                shellRowNum++;
                let bottomTimer = setInterval(function(){
                    shellRowNum++;
                    if($(`.body_row${shellRowNum}_td${shellTdNum}`).hasClass("tank") || shellRowNum === 1 || $(`.body_row${shellRowNum}_td${shellTdNum}`).hasClass("barrier")){
                        clearInterval(bottomTimer);
                        checkTank();
                        checkBarrier();  
                    }
                    runShell();
                }, 45);

            } else if(direction === "left"){
                shellTdNum--;
                let bottomTimer = setInterval(function(){
                    shellTdNum--;
                    if($(`.body_row${shellRowNum}_td${shellTdNum}`).hasClass("tank") || shellRowNum === 1 || $(`.body_row${shellRowNum}_td${shellTdNum}`).hasClass("barrier")){
                        clearInterval(bottomTimer);
                        checkTank();
                        checkBarrier();  
                    }
                    runShell();
                }, 45);

            } else if(direction === "right"){
                shellTdNum++;
                let bottomTimer = setInterval(function(){
                    shellTdNum++;
                    if($(`.body_row${shellRowNum}_td${shellTdNum}`).hasClass("tank") || shellRowNum === 1 || $(`.body_row${shellRowNum}_td${shellTdNum}`).hasClass("barrier")){
                        clearInterval(bottomTimer);
                        checkTank();
                        checkBarrier();  
                    }
                    runShell();
                }, 45);
            };

           const checkTank =()=>{
                let classList = $(`.body_row${shellRowNum}_td${shellTdNum}`).attr('class').split(/\s+/);
                $.each(classList, function(index, item) {
                    if(item.includes("user")){
                        $(`.${item}`).removeClass(`${item}`).removeClass("tank").css("background-color","white");
                    }
                });
            };

            const checkBarrier = () => {
                let classList = $(`.body_row${shellRowNum}_td${shellTdNum}`).attr('class').split(/\s+/);
                $.each(classList, function(index, item) {
                    if(item.includes("barrier")){
                        $(`.body_row${shellRowNum}_td${shellTdNum}`).removeClass("barrier").css("background-color","white");
                    }
                });
            }

            const runShell = () =>{
                setTimeout(function(){
                    $(`.body_row${shellRowNum}_td${shellTdNum}`).addClass(`shell_${name}`).css("background-color",`${color}`);
                },10);
                setTimeout(function(){
                    $(`.body_row${shellRowNum}_td${shellTdNum}`).removeClass(`shell_${name}`).css("background-color","white");
                },30);
            };
        }

        tankFire() {
            if($(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass(`${this.name}`) && $(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass(`${this.name}`)
            && $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).hasClass(`${this.name}`)){
                this.tankShell = "top";

            } else if($(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass(`${this.name}`) && $(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass(`${this.name}`)
            && $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum}`).hasClass(`${this.name}`)) {
                this.tankShell = "bottom";

            } else if(!$(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass(`${this.name}`)
                    && $(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass(`${this.name}`)){
                this.tankShell = "left";
                
            } else if(!$(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass(`${this.name}`)
                    && $(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass(`${this.name}`)){
                this.tankShell = "right";
            }
        }

        set tankMark(coordinate) {
            $(coordinate).addClass(`${this.name}`).addClass(`tank`).css("background-color",`${this.color}`);
        }

        set tankMarkOff(coordinate) {
            $(coordinate).removeClass(`${this.name}`).removeClass(`tank`).css("background-color",`white`);;
        }

        get renderTank() {
            this.tankRowNum = runRandom(3,47);
            this.tankTdNum = runRandom(3,47);
            this.coordinate = $(`.body_row${this.tankRowNum}_td${this.tankTdNum}`);  
            if(!$(`.body_row${this.tankRowNum}_td${this.tankTdNum}`).hasClass("tank") &&
                !$(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).hasClass("tank") &&
                !$(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).hasClass("tank") &&
                !$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`).hasClass("tank") &&
                !$(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`).hasClass("tank") &&
                !$(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).hasClass("tank")) {
                    this.tankMark = $(`.body_row${this.tankRowNum}_td${this.tankTdNum}`);
                    $(`.body_row${this.tankRowNum}_td${this.tankTdNum+1}`).css("background-color",`${this.color}`).addClass(`tank`).addClass(`${this.name}`);
                    $(`.body_row${this.tankRowNum}_td${this.tankTdNum-1}`).css("background-color",`${this.color}`).addClass(`tank`).addClass(`${this.name}`);
                    $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum+1}`).css("background-color",`${this.color}`).addClass(`tank`).addClass(`${this.name}`);
                    $(`.body_row${this.tankRowNum+1}_td${this.tankTdNum-1}`).css("background-color",`${this.color}`).addClass(`tank`).addClass(`${this.name}`);
                    $(`.body_row${this.tankRowNum-1}_td${this.tankTdNum}`).css("background-color",`${this.color}`).addClass(`tank`).addClass(`${this.name}`);
            } else {
                this.renderTank;
            }
        }
    }

    class Barrier {
        constructor(num){
            try {
               this.num = num;
            } catch(e) {

            }
        }

        renderBarrier(){                                                          
            rendBarrierAgain:for (let k = 0; k < this.num; k++) {
                let tankRowNum = runRandom(2,49); 
                let tankTdNum = runRandom(2,49);  
                if(!$(`.body_row${tankRowNum}_td${tankTdNum}`).hasClass('tank') 
                && !$(`.body_row${tankRowNum}_td${tankTdNum}`).hasClass('barrier')
                && !$(`.body_row${tankRowNum}_td${tankTdNum-1}`).hasClass('tank') 
                && !$(`.body_row${tankRowNum}_td${tankTdNum-1}`).hasClass('barrier')
                && !$(`.body_row${tankRowNum}_td${tankTdNum+1}`).hasClass('tank') 
                && !$(`.body_row${tankRowNum}_td${tankTdNum+1}`).hasClass('barrier')
                && !$(`.body_row${tankRowNum-1}_td${tankTdNum}`).hasClass('tank') 
                && !$(`.body_row${tankRowNum-1}_td${tankTdNum}`).hasClass('barrier')
                && !$(`.body_row${tankRowNum+1}_td${tankTdNum}`).hasClass('tank') 
                && !$(`.body_row${tankRowNum+1}_td${tankTdNum}`).hasClass('barrier')) {

                    $(`.body_row${tankRowNum}_td${tankTdNum}`).addClass('barrier');
                    $(`.body_row${tankRowNum}_td${tankTdNum+1}`).addClass('barrier');
                    $(`.body_row${tankRowNum}_td${tankTdNum-1}`).addClass('barrier');
                    $(`.body_row${tankRowNum-1}_td${tankTdNum}`).addClass('barrier');
                    $(`.body_row${tankRowNum+1}_td${tankTdNum}`).addClass('barrier');
                    $('.barrier').css("background-color","green");
                } else {
                    k--;
                    continue rendBarrierAgain;
                }
            }
        }
    }

    function runRandom(min, max) {
        let randomNum = parseInt(Math.random() * (max - min) + min);
        return randomNum;
    }

    $(`body`).on("keydown", function(e){
        let key = e.keyCode;
        setTimeout(moveRect(key), 5000);
    });

    $(`body`).on("keyup", function(e){
        if (e.keyCode === 32) {
            userAlex.tankFire();
        }
    });

    function moveRect(key){ 
        if(key === 38){
            userAlex.runTop();
        } else if (key === 37) {
            userAlex.runLeft();
        } else if (key === 40) {
            userAlex.runBottom();
        } else if (key === 39) {
            userAlex.runRight();
        }
    }

    const userAlex = new Tank('red','userAlex');
    const userKrisa = new Tank('black','userKrisa');
    const userZorik = new Tank('black','userZorik');
    const userPapik = new Tank('black','userPapik');
    const userFon = new Tank('black','userFon');

    userAlex.renderTank;
    userZorik.renderTank;
    userPapik.renderTank;
    userFon.renderTank;
    userKrisa.renderTank;

    const allBarier = new Barrier(20);
    allBarier.renderBarrier();
});
