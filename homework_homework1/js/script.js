const selectors = {
    miniMenu:'headerblock__mini',
    maxMenu:'headerblock__max',
    deactivMenu:'headerblock-deactiv',
    burgerBtn:'burger__button',
    burgerTogl:'burger__button_opened',
    miniNav:'headerblock__mininavbar',
    miniNavMen:'headerblock__mininavbar-menu',
    miniNavAct:'mininavbar-menu_active',
    velitLog:'velit-logo',
    dolorLog:'dolor-logo',
    proinLog: 'proin-logo',
    ametLog: 'amet-logo',
}


$(document).ready(function(){
    updateContainer();
    $(`.${selectors.miniNav}`).hover(function() {
        $(`.${selectors.burgerBtn}`).toggleClass(selectors.burgerTogl);
        $(`.${selectors.miniNavMen}`).toggleClass(selectors.miniNavAct);
    });

    $(window).on('resize', function(){
        updateContainer();
    }); 

    function updateContainer(){
        if ($(window).width() > 639 ){
            $(`#${selectors.velitLog}`).attr("src","img/velit_logo_640.png");
            $(`#${selectors.dolorLog}`).attr("src","img/dolor_logo_640.png");
            $(`#${selectors.proinLog}`).attr("src","img/proin_logo_640.png");
            $(`#${selectors.ametLog}`).attr("src","img/amet_logo_640.png");
        } else if ($(window).width() < 640) {
            $(`#${selectors.velitLog}`).attr("src","img/velit_logo.png");
            $(`#${selectors.dolorLog}`).attr("src","img/dolor_logo.png");
            $(`#${selectors.proinLog}`).attr("src","img/proin_logo.png");
            $(`#${selectors.ametLog}`).attr("src","img/amet_logo.png");
        };

        if ($(window).width() > 979) {
            $(`.${selectors.maxMenu}`).removeClass(selectors.deactivMenu);
            $(`.${selectors.miniMenu}`).addClass(selectors.deactivMenu);
        } else if ($(window).width() < 980) {
            $(`.${selectors.miniMenu}`).removeClass(selectors.deactivMenu);
            $(`.${selectors.maxMenu}`).addClass(selectors.deactivMenu);
        }

    } 

});
