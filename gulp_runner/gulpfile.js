var gulp        = require('gulp'),
	browserSync = require('browser-sync').create(),  
	csso = require('gulp-csso'), 
	concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    pump = require('pump');

gulp.task('start', function () {
    browserSync.init({
        server: {
            baseDir: "./../homework_homework2"
        }
    });
    gulp.watch("../homework_homework2/**").on('change', browserSync.reload);

})

gulp.task('csso', function () {                 
    return gulp.src('./../homework_homework3-js/css/*.css')
    	.pipe(concat('main.mini.css'))
        .pipe(csso())
        .pipe(gulp.dest('./../homework_homework3-js/dist/css')); //convert folder( remember "..")
});

gulp.task('js', function(cb) {  
     pump([
        gulp.src('../homework_homework3-js/js/script.js'),
        uglify(),
        gulp.dest('../dist/homework_homework3-js/js')  //convert folder( remember "..")
    ],
    cb
  );
});

function style() {
    return (
        gulp
            .src("./../homework_homework3-js/scss/*.scss")
            .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
            .pipe(gulp.dest("../homework_homework3-js/css"))
    );
}

function watch(){
    gulp.watch('../homework_homework2/scss/*.scss', style);
}

exports.style = style;
exports.watch = watch;  //scss convertor to css

gulp.task('convert', ['js', 'csso']);
gulp.task('default', ['start']);
