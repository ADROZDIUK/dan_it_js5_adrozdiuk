class Hamburger {
    constructor(size, stuffing) {
        try {
            if (arguments.length < 2) {
                throw new HamburgerException("Need two arguments, have " + arguments.length);
            }
            if (size === undefined) {
                throw new HamburgerException("no size given");
            }
            if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
                throw new HamburgerException("invalid size ");
            }
            if (stuffing === undefined) {
                throw new HamburgerException("no stuffing given");
            }
            if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_SALAD && stuffing !== Hamburger.STUFFING_POTATO) {
                throw new HamburgerException("invalid stuffing");
            }
            this._size = size;
            this._stuffing = stuffing;
            this.toppings = [];
        }   
        catch (e) {
            console.log(e.name + e.message);
        }
    }

    set addTopping (topping) {
        try {
            for (let i = 0; i < this.toppings.length; i++) {
                if(topping === this.toppings[i]){
                    throw new HamburgerException("Duplicate topping");
                }
            }
            if (topping !== Hamburger.TOPPING_SPICE && topping !== Hamburger.TOPPING_MAYO) {
                throw new HamburgerException("invalid topping");
            }
            if (topping === undefined) {
                throw new HamburgerException("topping not given");
            }
            if (arguments.length != 1) {
                throw new HamburgerException("Can add only one topping at a time, given: " + arguments.length)
            }
            this._topping = topping;
            this.toppings.push(this._topping);
        }  
        catch (e) {
            console.log(e.name + e.message);
        }
    }

    set removeTopping(topping) {
        try {
            if (arguments.length != 1) {
                throw new HamburgerException("Can remove only one topping at a time, given: " + arguments.length)
            }
            if(this.toppings.indexOf(topping) == -1) {
                throw new HamburgerException("Your burger don't have this topping");
            } else {
                this.toppings.splice(this.toppings.indexOf(topping),1)
            }
            if (topping === undefined) {
                throw new HamburgerException("topping not given");
            }
        }  
        catch (e) {
            console.log(e.name + e.message);
        }
    }

    findSize() {
        return this._size;
    }

    findStuffing() {
        return this._stuffing;
    }

    findToppings() {
        return this.toppings;
    }

    get calculatePrice() {
        let price = this._size.price + this._stuffing.price;
        if(this.toppings.length > 0){
            for (let i = 0; i < this.toppings.length; i++) {
                price += this.toppings[i].price;
            }
        }
        return price;
    }

    get calculateCalories() {
        let kk = this._size.kk + this._stuffing.kk;
        if(this.toppings.length > 0){
            for (let i = 0; i < this.toppings.length; i++) {
                kk += this.toppings[i].kk;
            }
        }
        return kk;
    }
}

function HamburgerException (message) {
    this.name = 'HamburgerException: ';
    this.message = message;
}

Hamburger.SIZE_SMALL = {price: 50, kk: 20};
Hamburger.SIZE_LARGE = {price: 100, kk: 40};
Hamburger.STUFFING_CHEESE = {price: 10, kk: 20};
Hamburger.STUFFING_SALAD = {price: 20, kk: 5};
Hamburger.STUFFING_POTATO = {price: 15, kk: 10};
Hamburger.TOPPING_MAYO = {price: 20, kk: 5};
Hamburger.TOPPING_SPICE = {price: 15, kk: 0};

// маленький гамбургер с начинкой из сыра
const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping = Hamburger.TOPPING_MAYO;
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories);
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice);
// я тут передумал и решил добавить еще приправу
hamburger.addTopping = Hamburger.TOPPING_SPICE;
// А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice);
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.findSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping = Hamburger.TOPPING_SPICE;
console.log("Have %d toppings", hamburger.findToppings().length); // 1
// не передали обязательные параметры
const h2 = new Hamburger(); // => HamburgerException: no size given
// передаем некорректные значения, добавку вместо размера
const h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE); 
// => HamburgerException: invalid size 'TOPPING_SAUCE'
// добавляем много добавок
const h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping = Hamburger.TOPPING_MAYO;
hamburger.addTopping = Hamburger.TOPPING_MAYO;
// HamburgerException: duplicate topping 'TOPPING_MAYO'
