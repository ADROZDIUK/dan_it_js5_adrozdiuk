$(document).ready(function(){
    $(`body`).append($('<table id="main-table"><tbody id="main-table__body"></tbody></table>'));
    for(let i = 0; i < 30; i++){
        $(`#main-table__body`).append($('<tr></tr>').addClass(`main-table__body_row${i+1}`));
        for(let j = 0; j < 30; j++) {
            $(`.main-table__body_row${i+1}`).append($('<td></td>'));
        }
    }
    $('body').on('click', function(event) {  //on body click
        let target = $(event.target);
        if(target.is("td")){
            $(target).toggleClass("td-black");
        } else if(target.is("body")) {  //tag <table> is table too ;p
            $('td').toggleClass("td-black");
        }
    });
});